<b><h1>INTRODUCTION</h1></b>


This script runs a simulation of clients been attended by a server.
It shows the results at the end of the simulation


<h2>PRE - REQUISITES</h2>

Python v2.7 already installed. If not, please install it.

Import the <b>Turtle Library</b> using the following command:

	----> pip install Turtle

<b><h1>SPANISH INSTRUCTIONS</h1></b>

Este script genera una simulación de una fila de clientes que son atendidos en un banco por un servidor.
Arroja los resultados al final de la simulación.

Como este es un script en python debe descargar python version 2 para
poder ejecutarlo.

Además, este script posee librerias para la parte de animación, por lo que
se deben descargar estos paquetes para su correcto funcionamiento.

Para instalar estos paquetes primero debe:

1--Tener Python v2.7 (versión 2.7) instalado en su computador

2--Abrir el CMD (línea de comandos) y ejecutar el siguiente comando:
		
	---->	pip install Turtle

<h3><b>If you found usefull this script, let me know. Also you can:</b></h3>
<b>Follow me on twitter:</b> <a href="https://twitter.com/FrostAbdiel">@FrostAbdiel</a>