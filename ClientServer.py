
"""
UNIVERSIDAD TECNOLOGICA DE PANAMA
FACULTAD DE INGENIERIA DE SISTEMAS COMPUTACIONALES
LICENCIATURA EN INGENIERIA DE SISTEMAS DE INFORMACION

SIMULACION DE SISTEMAS

ASIGNACION #3

Pertenece a:
            BERROCAL, ABDIEL

GRUPO: 1-IF-131
"""



"""
Importamos tres librerias:
                            turtle: lo usaremos para realizar la animacion de los clientes y el servidor
                            random: para generar valores aleatorios
                            sys (system): lo utilizaremos para mostrar los resultados de los calculos en pantalla

Para evitar nombres largos en el codigo utilizamos la funcion "as" en random para sustituir variables
"""

"""
Explicacion de colores de los clientes en la animacion:

    Rojo: un nuevo cliente a llegado, pero no esta en cola
    Azul: el cliente a pasado a la cola
    Verde: esta siendo atendido en el servidor
    Negro: representa al cliente que ya ha sido atendido
"""

from turtle import Turtle, mainloop, setworldcoordinates
import turtle as tur
from random import expovariate as randexp, random
import sys



def promedio(dato):
    """

    Este es un metodo y solamente lo utilizamos para calcular el promedio, por lo que lo creamos fuera de la clase
    ya que lo llamamos solo para realizar esa funcion
    """


    if len(dato) > 0:
        return sum(dato) / len(dato)
    return False

def iniciarLineas():
    """
    Esta funcion permite dibujar las lineas en la animacion
    """
    lineaColaArriba = tur.Turtle()
    lineaColaAbajo = tur.Turtle()
    tituloArrCliente = tur.Turtle()
    titulocolaCliente = tur.Turtle()
    tituloServidor1 = tur.Turtle()
    tituloServidor2 = tur.Turtle()
    tituloClieAtendidos = tur.Turtle()
    tituloAnimacion = tur.Turtle()

    lineaColaArriba.hideturtle()
    lineaColaArriba.penup()
    lineaColaArriba.right(50)
    lineaColaArriba.forward(60)
    lineaColaArriba.left(50)
    lineaColaArriba.forward(85)
    lineaColaArriba.pendown()
    lineaColaArriba.forward(25)

    lineaColaAbajo.hideturtle()
    lineaColaAbajo.penup()
    lineaColaAbajo.right(50)
    lineaColaAbajo.forward(80)
    lineaColaAbajo.left(50)
    lineaColaAbajo.forward(72)
    lineaColaAbajo.pendown()
    lineaColaAbajo.forward(25)

    tituloAnimacion.hideturtle()
    tituloAnimacion.up()
    tituloAnimacion.goto(50,0)
    tituloAnimacion.down()
    tituloAnimacion.write("Simulaci�n: 1 Cola - 1 Servidor (MM1)",font=("Arial",14,"bold"))

    tituloArrCliente.hideturtle()
    tituloArrCliente.up()
    tituloArrCliente.goto(0,-10)
    tituloArrCliente.down()
    tituloArrCliente.write("Arribo de clientes",font=("Arial",12,"normal"))

    titulocolaCliente.hideturtle()
    titulocolaCliente.up()
    titulocolaCliente.goto(95,-67)
    titulocolaCliente.down()
    titulocolaCliente.write("Cola de clientes",font=("Arial",12,"normal"))

    tituloServidor1.hideturtle()
    tituloServidor1.up()
    tituloServidor1.goto(150,-45)
    tituloServidor1.down()
    tituloServidor1.write("Servidor",font=("Arial",12,"normal"))
    tituloServidor1.right(90)
    tituloServidor1.forward(18)

    tituloClieAtendidos.hideturtle()
    tituloClieAtendidos.up()
    tituloClieAtendidos.goto(180,-100)
    tituloClieAtendidos.down()
    tituloClieAtendidos.write("Clientes Atendidos",font=("Arial",12,"normal"))

class Cliente(Turtle):
    """
    Para poder generar la animacion junto con los datos del programa se necesita heredar de la clase Turtle que se encuentra en
    la libreria importada con anterioridad
    Variables:
        lambda: es utilizado para el arribo de los clientes
        mu: es utilizado para el tiempo en el que el cliente es atendido
        velocidad_anim: es la velocidad de la animacion

    """


    def __init__(self, lmbda, mu, cola, servidor, velocidad_anim):



        Turtle.__init__(self)
        self.tiempo_entre_arribo = randexp(lmbda)
        self.lmbda = lmbda
        self.mu = mu
        self.cola = cola
        self.atendido = False
        self.servidor = servidor
        self.tiempo_servicio = randexp(mu)

        #Estos son funciones propias de la libreria Turtle para poder generar los graficos y la velocidad del mismo
        """
            Shape: es para darle forma al objeto, en este caso el cliente esta representado por un circulo
            Color: darle un color inicial al cliente cuando se encuentra antes de ir a posicionarse en la fila
            Speed: es la velocidad en que se movera los graficos o la animacion
        """


        self.shape('circle')
        self.color('red')
        self.speed(velocidad_anim)



    def movimiento_cliente(self, x, y):
        """
            Con esta funcion podremos realizar el movimiento de los clientes (en la animacion)
        """
        self.setx(x)
        self.sety(y)

    def arribo_cliente(self, t):
        """
        Funcion para el arribo del cliente (animacion)
        """

        self.penup()
        self.tiempo_arribo = t

        #estos son arreglos que nos permiten realizar el posicionamiento de los clientes
        #funciona como los FIFO (first in first out, entre otros)

        self.movimiento_cliente(self.cola.posicion[0] + 5, self.cola.posicion[1])

        self.color('blue')  #color que se asigna a los clientes que estan en cola

        self.cola.ingresar_cliente_cola(self)

    def inicio_servicio(self, t):
        """
        Inicio del servicio: con esta funcion hacemos que el cliente se mueve y la animacion lo presenta como el inicio de servicio
        """
        if not self.atendido:
            self.movimiento_cliente(self.servidor.posicion[0], self.servidor.posicion[1])
            self.hora_servicio = t + self.tiempo_servicio
            self.servidor.iniciar(self)

            self.color('green')  #este color lo asignamos para indicar que el cliente esta siendo atendido por el servidor

            self.fin_tiempo_cola = t

    def fin_servicio(self):
        """
        Fin del servicio: indica que el cliente ya ha sido atendido

        Esto se muestra en la animacion como un circulo (cliente) de color Negro
        """

        self.color('black') #asignacion del color
        self.movimiento_cliente(self.servidor.posicion[0] + 50 + random(), self.servidor.posicion[1] - 50 + random())
        self.servidor.clientes = self.servidor.clientes[1:]
        self.fin_tiempo_servicio = self.fin_tiempo_cola + self.tiempo_servicio
        self.tiempo_espera = self.fin_tiempo_cola - self.tiempo_arribo
        self.atendido = True



class Cola():
    """
    En esta clase programamos el comportamiento de la Cola
    Inicializamos la variable sacar = 0 para usarlo posteriormente
    """

    clientesAtendidos = 0

    def __init__(self, cola_posicion):
        self.clientes = []
        self.posicion = cola_posicion
    def __iter__(self):
        return iter(self.clientes)
    def __len__(self):
        return len(self.clientes)


    def sacar_cliente_cola(self, i):
        """
        Esto nos permitira retirar a un cliente de la cola cuando ha sido atendido
        """

        for p in self.clientes[:i] + self.clientes[i + 1:]:
            x = p.position()[0]
            y = p.position()[1]
            p.movimiento_cliente(x + 10, y)
        self.posicion[0] += 10

        #Cola.clientesAtendidos contiene la cantidad de clientes atendidas (cuando pasan a un circulo de color negro)

        Cola.clientesAtendidos += 1

        return self.clientes.pop(i)



    def ingresar_cliente_cola(self, cliente):
        """
        Es lo contrario que la funcion anterior. En esta se ingresa un nuevo cliente a la cola
        """
        self.clientes.append(cliente)
        self.posicion[0] -= 10

class Servidor():
    """
    Como lo dice el nombre. En la clase del Servidor se refleja el comportamiento del mismo cuando esta atendiendo a un cliente

    """
    def __init__(self, posicion_servidor):
        self.clientes = []
        self.posicion = posicion_servidor
    def __iter__(self):
        return iter(self.clientes)
    def __len__(self):
        return len(self.clientes)


    def iniciar(self,cliente):
        """
        Cuando se inicia el servicio a un cliente, este sale de la cola y se dirige al servidor
        Permite que se vea en la animacion cuando el cliente sale de la cola para ser atendido

        """
        self.clientes.append(cliente)
        self.clientes = sorted(self.clientes, key = lambda x : x.hora_servicio) #esta funcion de lambda (no es le mismo para los calculos)
                                                                                #es una funcion propia de python para realizar condiciones mas cortas
        self.siguiente_hora_servicio =  self.clientes[0].hora_servicio
    def servidor_sin_atender(self):
        """
        Esta funcion la creamos para indicar cuando el servidor esta libre y asi poder enviar los clientes
        """
        return len(self.clientes) == 0

class SimulacionBR():
    """
    Clase principal del programa
    """
    cantidadClientes = 0
    #se utiliza el __init__ en python para indicar el metodo principal o donde iniciar

    def __init__(self, Tiempo_simulacion, tiempo_entre_arribo, tiempo_servicio, velocidad_anim=6):




        """
        En esta seccion habilitamos el tamaño del cuadro o canvas donde se mostrara la animacion
        Siguiendo las coordenadas cartesianas x:horizontal y:vertical

        izqx: izquierda del eje horizontal
        derx: derecha del eje horizontal

        arry: arriba del eje vertical
        abajy: abajo del eje vertical

        Esto es para las funciones del Turtle

        """


        izqx = -10
        abajy = -110
        derx = 230
        arry = 5
        setworldcoordinates(izqx,abajy,derx,arry)
        cola_posicion = [(derx+izqx)/2, (arry+abajy)/2]



        self.Tiempo = Tiempo_simulacion
        self.completado = []

        self.tiempo_entre_arribo = tiempo_entre_arribo
        self.tiempo_servicio = tiempo_servicio
        self.clientes = []
        self.cola = Cola(cola_posicion)

        self.servidor = Servidor([cola_posicion[0] + 50, cola_posicion[1]])
        self.velocidad_anim = max(0,min(10,velocidad_anim))




    def nuevoCliente(self):
        """
        Funcion para la generacion de nuevos Clientes
        """
        if len(self.clientes) == 0:
            self.clientes.append(Cliente(self.tiempo_entre_arribo, self.tiempo_servicio, self.cola, self.servidor,self.velocidad_anim))
            SimulacionBR.cantidadClientes += 1



    def mostrarProgresoPantalla(self, t):
        """
        Con este metodo mostraremos las estadisticas que generaremos con los datos de la simulacion
        """

        sys.stdout.write('\n En Cola: %s \n' % len(self.cola)) #Muestra los clientes en cola durante la simulacion

        sys.stdout.write('\r%.2f%% (t=%s de %s) para finalizar la simulacion\n' % (100 * t/self.Tiempo, t, self.Tiempo))



    def ejecutar_simulacion(self):

        #Iniciamos con el tiempo en cero y creamos un nuevo cliente
        #Despues iremos con el siguiente, este iniciaria cuando termina el tiempo de servicio del anterior y asi sucesivamente
        #Luego creamos al cliente que debe esperar a ser atendido por el servidor
        iniciarLineas()

        t = 0
        self.nuevoCliente()




        siguienteCliente = self.clientes.pop()
        siguienteCliente.arribo_cliente(t)
        siguienteCliente.inicio_servicio(t)
        self.nuevoCliente()


        while t < self.Tiempo:
            t += 1
            self.mostrarProgresoPantalla(t)


            """
            Vamos a verificar si el servidor ya esta libre
            """

            if not self.servidor.servidor_sin_atender() and t > self.servidor.siguiente_hora_servicio:
                self.completado.append(self.servidor.clientes[0])
                self.servidor.clientes[0].fin_servicio()


                if len(self.cola)>0: #Importante saber si hay clientes en la cola

                    siguienteServicio = self.cola.sacar_cliente_cola(0)
                    siguienteServicio.inicio_servicio(t)
                    self.nuevoCliente()

            """
            Verificamos si hay cliente que han llegado
            """
            if t > self.clientes[-1].tiempo_entre_arribo + siguienteCliente.tiempo_arribo:
                siguienteCliente = self.clientes.pop()
                siguienteCliente.arribo_cliente(t)

                if self.servidor.servidor_sin_atender():
                    if len(self.cola) == 0:
                        siguienteCliente.inicio_servicio(t)
                    else:
                        siguienteServicio = self.cola.sacar_cliente_cola(0)
                        siguienteServicio.inicio_servicio(t)
            self.nuevoCliente()






    def Estadisticas(self):
        """

        En este metodo realizaremos algunas estadisticas que mostraremos en pantalla:

            --El promedio de Tiempo de espera
            --El tiempo promedio del sistema en funcionamiento
            --Cantidad Total de Clientes
            --Cantidad de Clientes Atendidos

        """

        self.tiempos_espera = []
        self.tiempos_servicio = []
        for p in self.completado:
            if p.tiempo_arribo >= 0:
                self.tiempos_espera.append(p.tiempo_espera)
                self.tiempos_servicio.append(p.tiempo_servicio)
        self.promTiempoEspera = promedio(self.tiempos_servicio)
        self.promTiempoSistema = promedio(self.tiempos_servicio) + self.promTiempoEspera
        sys.stdout.write("\n%sSIMULACI�N FINALIZADA%s\n\n" % (15*"-",16*"-"))
        ##########################################################################################
        #Estadisticas de la cola
        ##########################################################################################
        sys.stdout.write("\n%sESTADISTICAS DE LA COLA%s\n" % (14*"-",15*"-"))

        sys.stdout.write("TIEMPO PROMEDIO DE ESPERA: %.02f\n" % self.promTiempoEspera)

        #En la siguiente clase interna llamamos a la variable contadora de la Clase (SimulacionBR)
        #Para imprimir el valor que contiene la misma

        class obtenerCantClientes(SimulacionBR):
            cantClientes = SimulacionBR.cantidadClientes
            sys.stdout.write("Cantidad Total de Clientes: %s\n" % cantClientes)
        ##########################################################################################
        #Estadisticas del servidor
        ##########################################################################################

        sys.stdout.write("\n\n%sESTADISTICAS DEL SERVIDOR%s\n" % (13*"-",14*"-"))

        sys.stdout.write("TIEMPO PROMEDIO DEL SISTEMA: %.02f\n" % self.promTiempoSistema)


        #En la siguiente clase interna llamamos a la variable contadora de la Clase (Cola)
        #Para imprimir el valor que contiene la misma

        class obtenerCantAtendidos(Cola):
            cantidad = Cola.clientesAtendidos - 1
            if cantidad <= 0:
                cantidad = 0
                sys.stdout.write("Cantidad de Clientes Atendidos: %s\n" % cantidad)
            else:
                sys.stdout.write("Cantidad de Clientes Atendidos: %s\n" % cantidad)
        sys.stdout.write(39 * "-" + "\n")




"""
Se le pide al usuario que ingrese los datos para realizar la simulacion


Tiempo: es la cantidad de veces que es ejecutado la simulacion
tiempo_entre_arribo: es el tiempo en que llegaran los clientes
tiempo_servicio: tiempo de atencion del servidor
velocidad_anim: es la velocidad en que corre la animacion

"""
Tiempo = int(raw_input("Introduzca el tiempo de simulaci�n: "))
tiempo_entre_arribo = int(raw_input("Introduzca el tiempo de entre arribo: "))
tiempo_servicio = int(raw_input("Introduzca el tiempo de servicio: "))

"""
Quitar el comentario "#" para permirtir al usuario ingresar un valor para la
velocidad de la animacion de la simulacion
"""
#velocidad_anim = int(raw_input("Introduzca la velocidad de la animacion: "))

simIniciar = SimulacionBR(Tiempo, tiempo_entre_arribo, tiempo_servicio, 0.9)
simIniciar.ejecutar_simulacion()
simIniciar.Estadisticas()
